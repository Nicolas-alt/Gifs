import React, { useState, useEffect } from "react";
import CustomHelmet from "../components/Helmet/CustomHelmet";
import NavBar from "../components/GifUI/NavBar";
import GifItem from "../components/GifUI/GifItem";
import Footer from "../components/GifUI/Footer";
import Start from "../components/GifUI/Start";
import Chip from "../components/GifUI/Chip";
import { getGifs } from "../helpers/getGifs";
import "../assets/styles/Content.css";

const Content = () => {
  const [inputData, setInputData] = useState("");
  const [images, setImages] = useState([]);

  const handleInputData = (inputData) => setInputData(inputData);

  useEffect(() => {
    getGifs(inputData).then(setImages);
  }, [inputData]);

  const data = [localStorage.getItem("search")];
  JSON.parse(data);
  console.log(data);
  console.log(Array.isArray(data));

  return (
    <>
      <CustomHelmet title="Gif App" />
      <NavBar handleInputData={handleInputData} />
      <section className="section-results">
        {images.length > 0 ? (
          <>
            {data ? (
              <>
                <div className="div--chip">
                  <p>Watch again</p>
                  <div className="div--chipContainer">
                    {data.map((item) => (
                      <Chip name={item} key={item} />
                    ))}
                  </div>
                </div>
              </>
            ) : null}

            <section className="section--resultsItems">
              {images.map((image) => (
                <GifItem key={image.id} {...image} />
              ))}
            </section>
          </>
        ) : (
          <Start />
        )}
        <Footer />
      </section>
    </>
  );
};

export default Content;
