import React from 'react';
import NavBar from '../components/GifUI/NavBar';
import CustomHelmet from '../components/Helmet/CustomHelmet';
import '../assets/styles/About.css';

const About = () => {
  return (
    <>
      <CustomHelmet title="About" />
      <NavBar />
      <div class="div--about">
        <img
          src="https://avatars0.githubusercontent.com/u/52179095?s=460&u=3c32f626fcbc00f39db5da407a76ce7a0bb3e7f3&v=4"
          alt="Nicolas Picture"
        />
        <h2>Nicolas Jiménez </h2>
        <p>Front dev</p>
        <p class="p-about">
          This project was made by a passionate frontend developer from
          Colombia, 🇨🇴 You can find others projects
          <a href="https://github.com/Nicolas-alt">Here</a>
        </p>
        <hr />
        <div>
          <a href="https://twitter.com/Nicolas35103573">
            <i className="icon-footer bx bxl-twitter"></i>
          </a>
          <a href="https://github.com/Nicolas-alt">
            <i className="icon-footer   bx bxl-github"></i>
          </a>
        </div>
      </div>
    </>
  );
};

export default About;
