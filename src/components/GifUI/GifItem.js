import React from 'react';
import GifButtons from './GifButtons';
import '../../assets/styles/GifItem.css';
import PropTypes from 'prop-types'

export const GifItem = ({ title, url }) => {
  return (
    <div className="div--gif">
      <img className="img-gif" src={url} alt={title} />
      <div>
        <p>{title}</p>
        <GifButtons url={url} />
      </div>
    </div>
  );
};


GifItem.propTypes = {
  title: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired
}

export default GifItem;
