import React from 'react';
import {
  FacebookShareButton,
  FacebookMessengerIcon,
  TelegramShareButton,
  TelegramIcon,
  TwitterShareButton,
  TwitterIcon,
  VKShareButton,
  VKIcon,
  WhatsappShareButton,
  WhatsappIcon,
} from 'react-share';
import PropTypes from 'prop-types';

const GifButtons = ({ url }) => {
  return (
    <div className="div--Buttons">
      <FacebookShareButton url={url}>
        <FacebookMessengerIcon size={20} round={true} />
      </FacebookShareButton>
      <WhatsappShareButton url={url}>
        <WhatsappIcon size={20} round={true} />
      </WhatsappShareButton>
      <TelegramShareButton url={url}>
        <TelegramIcon size={20} round={true} />
      </TelegramShareButton>
      <TwitterShareButton url={url}>
        <TwitterIcon size={20} round={true} />
      </TwitterShareButton>
      <VKShareButton url={url}>
        <VKIcon size={20} round={true} />
      </VKShareButton>
    </div>
  );
};

GifButtons.propTypes = {
  url: PropTypes.string.isRequired,
};

export default GifButtons;
