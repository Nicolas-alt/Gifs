import React from 'react';

const Chip = ({ name }) => {
  return (
    <span>
      {name} <i className="bx bx-x"></i>
    </span>
  );
};

export default Chip;
