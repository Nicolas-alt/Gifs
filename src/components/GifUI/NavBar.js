import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types'
import '../../assets/styles/NavBar.css';

export const NavBar = ({ handleInputData }) => {
  const [inputValue, setInputValue] = useState('Search...');
  const [menu, setMenu] = useState(false);

  const handleToggleMenu = () => {
    setMenu(!menu);
  };

  const handleInputChange = (e) => {
    setInputValue(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    handleInputData(inputValue);
    if (localStorage.getItem('search')) {
      let data = localStorage.getItem('search');
      //console.log('local', data);
      data = JSON.parse(data);
      data.push({ title: inputValue });
      localStorage.setItem('search', JSON.stringify(data));
    } else {
      let search = [];
      search.push({ title: inputValue });
      localStorage.setItem('search', JSON.stringify(search));
    }
  };

  const handleClearInput = (_) => {
    setInputValue('');
  };

  return (
    <nav>
      <button className="button--navMenu" onClick={handleToggleMenu}>
        <i className="icon--buttonNav bx bx-menu"></i>
      </button>
      <ul>
        <li>
          <Link to="/" className="link--nav">
            GifsApp
          </Link>
        </li>
      </ul>
      <form
        onSubmit={handleSubmit}
        className={menu ? 'form--movil-active' : 'form--movil'}
      >
        <a className="a--navMenu" onClick={handleToggleMenu} href="">
          <i className="icon--navMenu-Close bx bx-menu-alt-left"></i>
        </a>
        <input
          type="text"
          name="search"
          value={inputValue}
          onChange={handleInputChange}
          onClick={handleClearInput}
        />
        <button onClick={handleSubmit}>
          <i className="icon--buttonNav bx bx-search"></i>
        </button>
      </form>
      <a href="https://github.com/Nicolas-alt/Gifs">
        <i className="icon--nav bx bxl-github"></i>
      </a>
    </nav>
  );
};

NavBar.propTypes = {
  handleInputData: PropTypes.func.isRequired
}

export default NavBar;
