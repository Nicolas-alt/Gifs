import React from 'react';
import logo from '../../assets/svg/logo.svg';
import '../../assets/styles/Start.css';

const Start = () => {
  return (
    <div className="div-logo">
      <img src={logo} className="start-logo" />
      <p>Ingresa una palabra para comenzar...</p>
    </div>
  );
};

export default Start;
