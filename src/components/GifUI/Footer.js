import React from 'react';
import { Link } from 'react-router-dom';
import '../../assets/styles/Footer.css';

const Footer = () => {
  return (
    <footer>
      <div>
        <a href="https://twitter.com/Nicolas35103573">
          <i className="icon-footer bx bxl-twitter"></i>
        </a>
        <a href="https://github.com/Nicolas-alt">
          <i className="icon-footer   bx bxl-github"></i>
        </a>
      </div>
      <Link to="/about">Created by Nicolas</Link>
    </footer>
  );
};

export default Footer;
