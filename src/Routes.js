import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import About from './pages/About';
import Content from './pages/Content';

const Routes = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Content} />
        <Route exact path="/about" component={About} />
        <Redirect to="/" />
      </Switch>
    </Router>
  );
};

export default Routes;
