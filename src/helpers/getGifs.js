// TODO: Cambiar y ocultar el api key
export const getGifs = async (inputData, limit = 10) => {
  try {
    const url = `https://api.giphy.com/v1/gifs/search?q=${encodeURI(
      inputData
    )}&api_key=PG4Rb06RwTLYuOSjl8FGDurURbSCnZxD&limit=${limit}`;
    const resp = await fetch(url);
    const { data } = await resp.json();
    const gifs = data.map((img) => {
      return {
        id: img.id,
        title: img.title,
        url: img.images?.downsized_medium.url,
      };
    });
    return gifs;
  } catch (error) {
    console.error(error);
  }
};
