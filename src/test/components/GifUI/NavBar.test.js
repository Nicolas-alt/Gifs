import React from 'react'
import { shallow } from "enzyme";
import NavBar from "../../../components/GifUI/NavBar";


describe('NavBar test', () => {
    const handleInputData = jest.fn()
    const wrapper = shallow(<NavBar handleInputData={handleInputData} />)

    test('Carga el componente adecuadamente', () => {
        expect(wrapper).toMatchSnapshot();
    });

    test('Input change', () => {
        const input = wrapper.find('input');
        const value = 'Value'
        input.simulate('change', { target: { value } })
    });

    test('Input submit', () => {
        wrapper.find('form').simulate('submit', { preventDefault() { } })
        expect( handleInputData ).toHaveBeenCalled()
    });

     test('Submit form', () => {
         const value = 'Value'
         wrapper.find('input').simulate('change', { target: { value } })
         wrapper.find('form').simulate('submit', { preventDefault() { } })
         expect(handleInputData).toHaveBeenCalled()
     });
});