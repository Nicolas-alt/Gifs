import React from 'react'
import { shallow } from "enzyme"
import GifItem from "../../../components/GifUI/GifItem"

describe('Prueba en <GifItem />', () => {
    const title = 'Gif';
    const url = 'http://gif.co'
    const wrapper = shallow(<GifItem title={title} url={url} />)

    test('Debe mostrar el componente', () => {
        expect(wrapper).toMatchSnapshot()
    });

     test('Debe tener una clase', () => {
         const div = wrapper.find('div').at(0);
         const className = div.prop('className')
         expect(className.includes('div--gif')).toBe(true)
     });

     test('La imágen debe tener un alt y una url', () => {
         const img = wrapper.find('img')
         expect(img.prop('alt')).toBe(title)
         expect(img.prop('src')).toBe(url)
     });

     test('Debe tener un parrafo', () => {
        const p = wrapper.find('p');
        expect( p.text().trim()).toBe( title );
     });
});
