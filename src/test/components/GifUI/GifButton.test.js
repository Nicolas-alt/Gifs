import React from 'react'
import { shallow } from "enzyme"
import GifButtons from '../../../components/GifUI/GifButtons'


describe('Test for <GifButton />', () => {
    const url = 'http://url.co'

    test('Prop types', () => {
        const wrapper = shallow(<GifButtons url={url} />)
        expect(wrapper).toMatchSnapshot()
    })
})