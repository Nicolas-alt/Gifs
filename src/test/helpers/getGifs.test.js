import { getGifs } from "../../helpers/getGifs";

describe('Get Gif helper', () => {
    test('Traer la data', async () => {
        const data = await getGifs('naruto')
        expect(data.length).toBe(10)
    });

     test('Error string', async () => {
         const data = await getGifs('')
        expect(data.length).toBe(0)
     });
});
