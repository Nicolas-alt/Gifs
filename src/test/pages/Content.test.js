import React from 'react'
import { shallow } from 'enzyme';
import Content from '../../pages/Content';

describe('Content Test suite', () => {
    const wrapper = shallow(<Content />)
    
    test('Se muestra el componente', () => {
        expect(wrapper).toMatchSnapshot()
    });
});
